01
    instala node e vs

02
    npm init na pasta
        npm initi ele vai ficar perguntando as infos do packdge.json
        npm initi -y ele roda direto 

    cria um js e da um .\node index.js
    use strict - força o js a ser mais criterioso

    node .\nomeArquivo

    fala de servidor
        instala http express debug
            npm install http express debug --save

03
    servidor e postman
    
    importa o http, o debug e o express
    no node é: const nomeVariavel = require(moduloImportado)
    tudo que for colocado entre aspas simples ele vai procurar node modules
    o que tiver um caminho ele procura no caminho
        pra isso, tem q iniciar com um .\

    na app da um const app = express()
        onde app é uuma variavel, pode ter qq nome, mas o mais comum é app
    
    const port = numeroDaporta
    ap.set('port', port)

    com a pp instanciada, cria o servidor:

    const server = http.createSwrver(app)
    const router = express.Router()

    ele configura uma rota pra aplicação rodar

    e por fim, fala pro sserv ficar ouvindo a porta
    server.listen(port)

    postman
        simula requisições

04
    normalizando porta
    se deixarmos uma porta hardcodde pode dar problema na hora de subir
    local funciona pq a porta ai ta liberada, mas qd subir a app, pode ser q no serv nao esteja

    dae ele cria uma function p tratar isso
    ve se tem umaporta disponível, se não tiver, usa a 3000

05
    debug
    erros do servidor
    cria a function em q pega as infos do ser e starta o debug
    depois ele chama a function junto com o error
    do mesmo jeito q tem o evento error, tem o evento listen
    server.on('listening', nomeDaFunction)

7
    separar rota do servidor
    
    cria a pasta bin e poe um .js la dentro
    ele leva tudo e inica a separação po la

    modificações no server.js
        require app na primeira linha
        é onde ficaram as rotas, é o index.js
        no meu caso é require index.js na primeira linha


    cria uma pasta src e ali dentro bota o index.js
    e la ficam:
        require express
        const  app = express
        const rota = express.Router()

        e a rota

        e no final exporta a rota 

        module.exports = app

        no terminal roda o server


passo a passo primeira parte
    cria a pasta
    npm init -y
    
    instala 
        http
        express
        debug
        npm install http express debug --save

    rota
        importa a lib - require express

        coloca o express numa var p trabalhar com os metodos
            const app = express();

        coloca o .Router numa variavel
            const router = express.Router();

        configura a rota
            é o const route = router.get('/', (requ, res ) => {//do sonthing})
                res.status(200).send({
                    title: "Node start api",
                    version: "0.0.1"
                }); --->> exemplo de código para aplicação rodar
        
        da um app use - app.use("/", route);

        exporta o app

    servidor
        importa as libs
        inporta a rota (app)
        const nomeqq = require('') - http, debug

        seta a porta
            const port = numeroDaporta
            ap.set('port', port)

        com a porta criada, levanta o servidor e aponta a rota
            const server = http.createServer(app)
        
        fala pro serv ouvir a porta
            server.listen(port)
        
        por fim da um cosole.log com qqmsg só p vr se ta tudo ok


        apendices:
            deixar porta dinamica
            debug servidor
            mongo - ./mongod.exe

    vai no pakge.jason, em scripts e coloca "start": "node ./bin/server.js", da da p rodar com npm start


8
    npm start
    no packge.json, em scripts adiciona
        "start":"node .\bin\server" 
        no terminal dai é só dar um npm start

9
    nodemon
        reseta o servidor sempre que um arquivo é alterado
        é pra nao ter que ficar danco um crtl+C p para e depois um npm start
        o nodemon faz isso p nos

        ele fica observando a pasta

        npm install nodemon --save--dev
        npm install -g nodemon

        --save - escrve no dependencies
        --save-dev - escreve no devDependencies
        
        depois q instalou, vai no terminal e da um nodemon caminhoDoServ
            no caso desse proj - nodemon .\bin\server.js

10 - crud
    creat, update, delete

    ele iniciao fazendo novas rotas
    a primeira, create, executano um post
        const creat = router.post('/' () => {})
        no res.send() ele manda uma info diferente 

    qd criamos uma rota tmos diversas opções. Post, get, put, delte, ....
        post cria
        put atualiza

    podemos ter a mesma rota '/' distribuindo p diferentes "metodos"

    na entrada da function temos os 3 parametros, req, res, next
    requisição
        qd fazemos um get, ou um post, estamos fazendo uma req
        ela gera uma resposta (res)
        o next é p prosseguir a execussao, nao vamos utiliza-lo, por hora

        sempre tem q ter o res.algumacoisa
        sem o res o servidor nao envia a resposta

        enviamos o status e um send
            status:
                200 - ok 
                201 - created
                400 - erro, bad request
                401 - nao autenticado


        ver como ta o de casa e mergiar direito em casa a aula 10
            baixa essa branch em casa
            copia a aula 10 pra ca
            mergeia essa e lima o q tem em casa
            o q twm la, tem aqui. Só as infos do reademe q o de casa ta melhor q esse.

11 - pasta rotas
    dentro do src cria uma poasta, chama de routes
    ele cria um arquivo sreparado para a rota principal, o index.js
    essa rota é tem um intuito de teste, ele coloca informações randomicas
    titulo da api, versao, .....

    nesse index.js importa o express, e da um const router = express.Router()

    depois vai a rota

    depois exporta a rota

    dae da pra remover a rota o app file

    Depois de exportar o index da pasta rota, tem q importar o app file

    const variavelQq = require('caminho/do/arquvivoRota')
    const rotaIndex = reqquire('./routes/index'); 

    no app file, onde agente chama as rotas:
        app.use("/", route);
        app.use("/products", create);
        app.use("/products/", put);
        app.use("/", del);
    Nesse trecho, a primeira linha
        qd a gente tinha a rota no app file, ela se chama route
        agora q estamos importando ela se chama index - ver linha do import
        entao trocar route por index, fica assim:
            app.use("/", rotaIndex);
            app.use("/products", create);
            app.use("/products/", put);
            app.use("/", del);
    testei via postman e rolou

    depois cria um arquivo products.js na pasta routes
    e leva pra la as rotas dos produtos
        na base do ctrlx ctrlv

    O mesmo esquema da primeira rota
    faz os dois imports la em criamos
    poe as rotas
    exporta o route
    vai no app file e importa o products route

12 - controllers
    a mao toda de trazer do banco e mandar pro banco faria os arquivos de rota ficarem mt grandes
    por isso é boa pratica separar o roteamento da logica da aplicação

    as rotas ficam nos arquivos de rota e a logica fica nas controllers

    na pasta src cria uma pasta controolers
        e dentro dela cria um arquivo products-controller.js

        na controller products coloca o metodo

        depois vai na rota, tira a logica e chama a controller
            pra isso:
            1 - importa a controller
            2 - muda a rota:
                rera assim:
                    router.post("/", (req, res, next) => {
                        res.status(200).send({
                        msg:"POST",
                        body: req.body
                        });
                    });

                fica assim:
                    router.post("/", controller.post);

        Repete a ação com o put e o delete

13 - mongo db setup
    fala do mongo, mas diz q o curso será feito com um banco online, o mlab
    diz que é mais negocio para o curso. 
    qd chegar a hora de publicar o projeto o banco ja ta online

    clica em creaate new
    clica em sandbox
    segue todo o passo a passo até criar o banco
    com o banco criado, cria um user

    com o usuario criado, o mlab apresenta a conection string

    baixa o studo 3t
    cria em create conection
    cica em from uri
    cola a conection string do mlab
    coloca user e senha na string
    testa a conection
    se tiver ok, salvab
    qd aperecer a lista de conexoes, clica duas vezes em cima dela

14 - mongose
    instala o mongoose
    vai no app file p instanciar o mongo e abrir a conexao
    
    importa o mongose normal la em cima

    e antes da rota poe um - mongoose.connect('conection string');
    é a conection string do mlab

15 - models
    com o banco criado o negocio agora é criar coleções e ver como manipula-las
    cria uma nova pasta - models
    cria um arquivo alidentro, o products.js

    banco nosql sao schemaless
    tipo casa de irene
    da por titulos repetidos, ids repetidos, nao tem tipagem de dados,..... pq nao é relacional
    com o mogoose criamos schemas para fazer a validação dos dados
    assim o banco sai do base e vem para a aplicação
    Uma das desvantagens do nonsql

    para criar um schema file (no nosso caso o products dentro da models) é o seguinte:

    importa o mongose 
    e da um const schema = mongoose.schema

    dae cria um novo schema

    no final exporta o schema
    module.exports = mongoose.model('Product', schema);  
    é no exports que a gente da um nome pro model 

16 - utilizando o modelo criado
    vai na controller e importa a colection e o monngoose
    o mongoose é um import normal, e a model tem o seguinte:

    tipo
        temos uma controller produto, importa a model produto
        se tivermos varias controllers, importamos só a(s) model(s) que essa controller controla
        numa controller user nao pq importar a model produtos

    linha do import da model:
    const Product = mongoose.model('Product');

    a partir da qui ele da a barbada de como criar products
    o que em um sql seria o insert
    entao no post, antes od res. bal bla bla vai a logica
    cria uma nova instancia do produto, dizendo de onde vem os dados
    nesse caso do curso do req.body

    depois da um product.save
    o .save é o metodo do mongo/,mogoose que escreve no banco
    é ele que vai botar o produto no banco

    aqui tem que se ligar no seguinte
    js é assincrono
    entao o programa executa o .save e nao espera retorno
    ele manda a promessa de um retorno
    p fazer ele esperar o retorno tem que trabalhar com async await

    por isso o retorno vai pra dentro do .then()

    importa a models no app model

    ele faz uma requisição via postamn pra falar sobre as msgs de erro.

17 - listando produtos
    na controller cria uma funcao exports.get

    ali faz a logica

    o metodo p bucar no banco é o .find()

    find all: .find({});
    parametros: .find({title: "xpto"})
                .find({_id: '213454'})
                .find({lorem:"açbua", impsum:"ddddd"})

    depois do find tem o .then() com o sucess e erro (catch);

    na controller ele criou um metodo para o get
    nao vamos mais usar aquele get geralzao.
    entao na rota ele cria uma rota get apontando p controller, pro metodo get 
    dae no postman ou app, se ligar q o endpoit nao é mais /, e sim /products
    o / ativa o get geralzao, o /products ativa o get products

    no resultado da pesquisa vieram campos desncessarios, como versao, id,...
    da pra configurar quais os campos que desejamos retornar para a a aplicação
    .find({'parametros ou nao'}, 'campos que retornam p aplicação'),then()........
    .find({}, 'title price tags') - separados por um espaço em branco

18 - get by slug
    o memso metodo do primeiro get by id
    passando o slug como parametros

    qd damos u .find(), o retorno é um array
    mas o slu é um campo unique, só volta um, sempre.
    entao é mais negocio da um .findOne(), q retorna um obj ao inves de um array


19 - find by id
    da um findById(id)

    no arquivo de rotas teve um conflito
    /:id e /:slug

    pra crrigir muda uma das rotas
    
    /qq_coisa/:parametros
    /:parametro
    assim sao duas rotas diferentes e nao conflitam

20 - get by tag
    o mogoose tem um metodo que busca por tag

    as tags sao um array
    se fossemos fazer na mao teriamos que:
         pegar o prod
         percorrer o array das tags p ver alguma bate com o a tag q foi passada por parametro

    pra deixar isso pro mongoose é o seguinte:
    da um .find() normal
    e nos parametros poe tags:req....tags      
    
21 - put
    é o update
    atualiza o registro ja existente

    as nformações vem pelo body da requisição

    no rotas ja tem o put - fica como ta

    o mongoose tem um metodo, o findById and Update
    chama esse cara passando o id
    no $set diz quais os valores q vaoser atualizados e de onde vem o valor novo

22 - delete
    o mongoose tem o metodo findone and remove

    passa o is q vem da requisicao

    no arquivo rota nao tem parametro id ainda
    da pra por na url ou da pra madar pelo body
    deixei na url

23 - validações
    nao curti
    ele nao fala das validações do mongoose, aplica um arquivo de validação que ele fez
    o arquivo é simples
    ele cria um array vazioe vai testando cada campo
    se algum campo der erro ele da um push com a msg no array e retorna o array com as msgs de erro
    ele fez esse arquivo pra levar os "if´s" para um unico arquivo de validações
    assim eles nao ficam poluindo a model

    depois de criar o file validation, importa ele na controller
    em cada mtodo valida os campos

    é ruim, pega um form grande, em cada metodo tem que ficar validando cada campo.

    **ir atras de validações default do mongoose

    Mongoose nao valida os campos direito, toma no cu
    a function do balta é ruim pelo seguinte:
        no metodo post
            pega o campo title
                valida tipo
                valida isso
                valida aquilo
                valida size
                ....
            pega o campo slug
                valida tipo
                valida isso
                valida aquilo
                valida size
                ....
            
            faz isso em cada campo
        depois vai pra outro metodo (put, por exemplo) e valida tudo de novo.
        Impraticável
        Se for fundamental trabalahr com validações, o melhor negocio é meter um hapi e joi

        por enquanto o projeto vai ficar sem validação p nao ficar tomando arrasto com isso

24 - repoistory patterns
    camada de acesso ao banco
    a controller fica com a logica e o repository faz os .save(), .find(), .findOne(), ....
    Um repositorio em um projeto pqeuno assim nao é impactante, mas emgrandes projetos eles são ajudam a organizar o codigo

25 - async/await
    torna a execução sincrona
    Não curti
    a aplicação deixa de ser reativa e passa a sr imperativa
    nao gera uma economia de codigo tao significativa
    entendi o conceito, mas vou manter a pp assincrona

26 - model customer
    Criar schema file:
        na pasta models cria um arquivo customer.js
        
        importa o mongoose
        const mongoose = require('mongoose');

        Cria um mongoose.Schema
        const Schema = mongoose.Schema;

        Cria um novo schema
        const schema = new Schema({
            atributos vao aqui
        });

        exporta o schema
        module.exports = mongoose.model('Product', schema);

    os lugares onde normalmente se importa o schema criado:
        app file
        controlers e repositorios que vao trabalharcom esse schema

27 - model order

28 - crontroller customer.


CRUD FINALIZADO
PARTIU FAZER A INTERFACE



